# Welcome to the Official Iterum Modpack Page
This is a highly optimized modpack for **1.20.4**, with mods such as 'Lithium', 'Sodium', etc. Made especially for those with not so powerful computers to run **Minecraft 1.20.4** with no bottlenecks!

This modpack was made for members of the Iterum SMP, but if you're not one feel free to download as well! We hope you have a lot of fun!

## Mod List
+ 3D Skin Layers
+ Armor Stands
+ BetterF3
+ Bobby
+ Capes
+ Chat Heads
+ Cloth Config API
+ Continuity
+ Dynamic FPS
+ Entity Culling
+ Fabric API
+ Fabric Language Kotlin
+ FerriteCore
+ Freecam (Modrinth Edition)
+ Fusion (Connected Textures)
+ ImmediatelyFast
+ Indium
+ Iris Shaders
+ Krypton
+ LazyDFU
+ Lithium
+ Memory Leak Fix
+ Mod Menu
+ ModernFix
+ More Culling
+ No Chat Reports
+ Shulker Box Tooltip
+ Simple Voice Chat
+ Sodium
+ Sound Physics Remastered
+ Status
+ SuperMartijn642's Core Lib
+ ThreadTweak
+ View Distance Fix
+ Wavey Capes
+ YetAnotherConfigLib
+ Your Options Shall Be Respected (YOSBR)
+ Zoomify

## What is Iterum?
**Iterum** is a Brazilian SMP, created by [@saamoff](https://modrinth.com/user/saamoff) and [@Kaidxn](https://modrinth.com/user/Kaidxn), where a variety of content creators come togheter to create awesome stuff and play minecraft togheter! Currently were on the First Season of the Server. If you're interested heres a link to the [Official Iterum X profile](https://twitter.com/iterumsmp).